const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/Product.js");

module.exports.registerUser = (requestBody) => {
	let newUser = new User({
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10)
	})
	return newUser.save().then((user, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.allUsers = () => {
	return User.find({}).then(result => {
		result.password = " ";
		return result;
	})
}

module.exports.authenticateUser = (requestBody) => {
	return User.findOne({email: requestBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

