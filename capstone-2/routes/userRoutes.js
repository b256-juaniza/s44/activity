const express = require("express");
const router = express.Router();
const userController = require("../controller/userController.js");
const auth = require("../auth.js");

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/allUsers", (req, res) => {
	userController.allUsers().then(resultFromController => res.send(resultFromController));
})

router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));

})

module.exports = router;